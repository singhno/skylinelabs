package com.androidbelieve.drawerwithswipetabs;

/**
 * Created by USER on 04-06-2016.
 */
public class Movie {
    private String title, genre, year;
    int imageid;

    public Movie() {
    }

    /*
    public Movie(String title, String genre, String year) {
        this.title = title;
        this.genre = genre;
        this.year = year;
        this.imageid = -1;
    }*/

    public Movie(String title, String genre, String year, int imageid) {
        this.title = title;
        this.genre = genre;
        this.year = year;
        this.imageid = imageid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getImageid() {
        return imageid;
    }

    public void setImageid(int imageid) {
        this.imageid = imageid;
    }

}