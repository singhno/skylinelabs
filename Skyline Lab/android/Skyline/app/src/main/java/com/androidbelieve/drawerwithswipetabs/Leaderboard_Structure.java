package com.androidbelieve.drawerwithswipetabs;

/**
 * Created by Shayan Anwar on 6/18/2016.
 */
public class Leaderboard_Structure {
    private String name;
    int rank,points;
    public Leaderboard_Structure() {
    }

    public Leaderboard_Structure(String name, int rank, int points) {
        this.name = name;
        this.rank = rank   ;
        this.points = points;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getPoints() {
        return points;
    }

    public void setGenre(int points) {
        this.points = points;
    }
}