package com.androidbelieve.drawerwithswipetabs;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.androidbelieve.kale.mqtt.Connection;
import com.androidbelieve.kale.mqtt.Connections;

/**
 * Created by Shayan
 */
public class SocialFragment extends Fragment {


    View a;
    FloatingActionButton b;
    private final int qos = 0;
    Connections connections;
    ImageButton ib;
    TextView tv;
    ToggleButton tb;
    Connection connection;
    String voice;


    public SocialFragment() {
        // Required empty public constructor
        connections = new Connections(null);
        connection = connections.getInstance().getConnection("client");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View a =  inflater.inflate(R.layout.social_layout, container, false);
        b = (FloatingActionButton) a.findViewById(R.id.fab_kitchen);
        tv = (TextView)a.findViewById(R.id.tempkitchen);
        tb = (ToggleButton) a.findViewById(R.id.tb_kitchen);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i  = new Intent(getActivity(),Appliances_Kitchen.class);
                startActivity(i);
            }
        });

        tb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tb.isChecked()) {
                    connection.subscribe(getString(R.string.temperature), qos);
                } else {
                    connection.unsubscribe(getString(R.string.temperature));
                }
            }
        });
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getActivity(), Appliances_Kitchen.class);
                startActivity(i);
            }
        });
        return a;
    }


    public void setTectView(String s) {
        tv.setText(s);
    }
}