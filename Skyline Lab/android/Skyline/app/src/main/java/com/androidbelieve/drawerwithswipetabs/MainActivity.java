package com.androidbelieve.drawerwithswipetabs;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.androidbelieve.kale.mqtt.Connection;
import com.androidbelieve.kale.mqtt.Connections;
import com.androidbelieve.kale.mqtt.MqttCallBackHandler;

import org.eclipse.paho.client.mqttv3.MqttConnectOptions;

public class MainActivity extends AppCompatActivity{
    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    TabFragment tf ;
    Connections connections;
    SharedPreferences sp;
    Connection connection, service;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sp = getSharedPreferences("switch_state", MODE_PRIVATE);
        connection = new Connection(this, getString(R.string.ip), getString(R.string.port), "client") {
            @Override
            public void Connected(boolean isConnected) {

            }
        };
        connections = new Connections(this);
        service = connections.getInstance().getConnection("service");
        if(sp.getBoolean("safety", false)) {
            service.subscribe("ir", 0);
        }
        if(sp.getBoolean("touch", false)) {
            service.subscribe("touch", 0);
        }
        MqttConnectOptions mo = new MqttConnectOptions();
        mo.setMqttVersion(MqttConnectOptions.MQTT_VERSION_3_1);
        connection.setMqttConnectOptions(mo);
        connection.CreateConnection();
        connection.getClient().setCallback(new MqttCallBackHandler() {
            @Override
            public void messageRecieved(String s) {
                tf.setText(TabFragment.PRIMARY_FRAGMWNT, s);
                tf.setText(TabFragment.KITCHEN_FRAGMWNT, s);
                tf.setText(TabFragment.BEDROOM_FRAGMWNT, s);
            }
        });
        connection.connect();
        connections.getInstance().addConnection(connection);
        /**
         *Setup the DrawerLayout and NavigationView
         */

        tf = new TabFragment();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
             mNavigationView = (NavigationView) findViewById(R.id.shitstuff) ;

        /**
         * Lets inflate the very first fragment
         * Here , we are inflating the TabFragment as the first Fragment
         */
             mFragmentManager = getSupportFragmentManager();
             mFragmentTransaction = mFragmentManager.beginTransaction();
             mFragmentTransaction.replace(R.id.containerView,(tf)).commit();

  /* Setup click events on the Navigation View Items.
         */

             mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
             @Override
             public boolean onNavigationItemSelected(MenuItem menuItem) {
                mDrawerLayout.closeDrawers();

/* yahan par navbar ke listeners hain neeche */

               /* if (menuItem.getItemId() == R.id.nav_insights) {
                     FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.containerView,new SocialFragment()).commit();

                 }*/

                 if (menuItem.getItemId() == R.id.nav_settings) {
                     Intent intent = new Intent(MainActivity.this,Settings.class);
                     startActivity(intent);

                 }


                 if (menuItem.getItemId() == R.id.nav_sharecontrol) {
                     FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                     fragmentTransaction.replace(R.id.containerView,new SocialFragment()).commit();

                 }


                 if (menuItem.getItemId() == R.id.nav_aboutus) {
                     FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                     fragmentTransaction.replace(R.id.containerView,new SocialFragment()).commit();

                 }


                 if (menuItem.getItemId() == R.id.nav_livingroom) {

                     Intent intent = new Intent(MainActivity.this,MainActivity.class);
                     startActivity(intent);

                 }

                 if (menuItem.getItemId() == R.id.nav_kitchen) {
                     FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                     fragmentTransaction.replace(R.id.containerView,new SocialFragment()).commit();

                 }


                 if (menuItem.getItemId() == R.id.nav_bedroom) {
                     FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                     fragmentTransaction.replace(R.id.containerView,new UpdatesFragment()).commit();

                 }


                 if (menuItem.getItemId() == R.id.nav_logout) {

                     Intent intent = new Intent(MainActivity.this,App_intro.class);
                     startActivity(intent);
                 }

                 if (menuItem.getItemId() == R.id.leaderboard) {

                     /*Intent intent = new Intent(MainActivity.this,Leaderboard_Main.class);*/
                     Intent intent = new Intent(MainActivity.this, Leader_Board.class);
                     startActivity(intent);
                 }

                 if (menuItem.getItemId() == R.id.dataanalytics) {

                     /*Intent intent = new Intent(MainActivity.this,Leaderboard_Main.class);*/
                     Intent intent = new Intent(MainActivity.this, Data_Analytics.class);
                     startActivity(intent);
                 }






                 return false;
            }

        });

        /**
         * Setup Drawer Toggle of the Toolbar
         */

                android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
                ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this,mDrawerLayout, toolbar,R.string.app_name,
                R.string.app_name);

                mDrawerLayout.setDrawerListener(mDrawerToggle);

                mDrawerToggle.syncState();

    }





}