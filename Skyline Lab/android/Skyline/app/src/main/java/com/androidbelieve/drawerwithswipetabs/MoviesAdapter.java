package com.androidbelieve.drawerwithswipetabs;

/**
 * Created by USER on 04-06-2016.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.Preference;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidbelieve.kale.mqtt.Connection;
import com.androidbelieve.kale.mqtt.Connections;

import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MyViewHolder> {
    SharedPreferences sp;
    private List<Movie> moviesList;
    private int mposition;
    private final int qos = 0;
    private Connection c;
    Connections connections;
    Context context;
    SharedPreferences.Editor e;
  //  EditText test;





    public MoviesAdapter(Context context, List<Movie> list) {
        connections = new Connections(context);
        c = connections.getInstance().getConnection("client");
        this.context = context;
        moviesList = list;
        sp = context.getSharedPreferences("switchstate",Context.MODE_PRIVATE);
        e = sp.edit();

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public SwitchCompat ff, gh;
        public TextView title, year, genre;
        public CircleImageView civ;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            genre = (TextView) view.findViewById(R.id.genre);
            year = (TextView) view.findViewById(R.id.year);
            civ = (CircleImageView) view.findViewById(R.id.profile_image);

            ff = (SwitchCompat) view.findViewById(R.id.auto);
            gh = (SwitchCompat) view.findViewById(R.id.togg);



        }
    }


    public MoviesAdapter(List<Movie> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    public void ActivityDestroy() {
        e.commit();
    }

    boolean check;
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        Movie movie = moviesList.get(position);
         holder.title.setText(movie.getTitle());
        holder.genre.setText(movie.getGenre());
        holder.year.setText(movie.getYear());
        holder.ff.setTag("sen" +movie.getTitle() +"_auto");
        check = sp.getBoolean("sen" +movie.getTitle() +"_auto", false);
        holder.ff.setChecked(check);
        holder.gh.setTag(movie.getTitle());
        holder.civ.setImageResource(movie.getImageid());
        check = sp.getBoolean(movie.getTitle(), false);
        holder.gh.setChecked(check);
        holder.gh.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String topic = (String)  buttonView.getTag();

                if (c == null) {
                    Toast.makeText(context, "Connection is null", Toast.LENGTH_SHORT).show();

                } else {
                    String message;
                    if (isChecked) {
                        message = "1";
                        e.putBoolean(topic, true);
                    } else {
                        message = "0";
                        e.putBoolean(topic, false);
                    }
                    if (c.getCOnnectionStatus()) {
                        Toast.makeText(context, topic, Toast.LENGTH_SHORT).show();
                        c.publish(topic.toLowerCase(), message, qos);
                    } else {
                        Toast.makeText(context, "Through here", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        holder.ff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String topic = (String)  buttonView.getTag();

                if(topic.toLowerCase().contains("senlight_auto")) {
                    ;
                }
                else {
                    return;
                }
                if (c == null) {
                    Toast.makeText(context, "Connection is null", Toast.LENGTH_SHORT).show();

                } else {
                    String message;
                    e.putBoolean(topic, isChecked);
                    if (isChecked) {
                        message = "1";
                        e.putBoolean(topic, true);
                    } else {
                        message = "0";
                        e.putBoolean(topic, false);
                    }
                    if (c.getCOnnectionStatus()) {
                        Toast.makeText(context, topic, Toast.LENGTH_SHORT).show();
                        c.publish(topic.toLowerCase(), message, qos);
                    } else {
                        Toast.makeText(context, "Through here", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}





