package com.androidbelieve.drawerwithswipetabs;


import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class LeaderboardAdapter extends RecyclerView.Adapter<LeaderboardAdapter.MyViewHolder> {

    private List<Leaderboard_Structure> moviesList;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, rank, points;
        public CardView mCardView;
       // public RecyclerView mRecyclerView;

        public MyViewHolder(final View view) {
            super(view);
            int pos = getAdapterPosition();
            this.name = (TextView) view.findViewById(R.id.name);
            this.rank = (TextView) view.findViewById(R.id.rank);
            this.points = (TextView) view.findViewById(R.id.points);
            this.mCardView = (CardView) view.findViewById(R.id.leaderboard_card);


           // mCardView. setCardElevation(34);
            //if(pos == 2){
               // mCardView.setBackgroundColor(0xff0000ff);
               // Toast.makeText(view.getContext(), String.valueOf(pos), Toast.LENGTH_SHORT).show();
            //}
            //this.mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_leaderboard);

            mCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                   //int item = moviesList.get(ItemP)
                    //Toast.makeText(view.getContext(), String.valueOf(pos), Toast.LENGTH_SHORT).show();
                    if(pos == 2){
                        name.setText("User's name");
                    }
                }
            });


        }
    }


    public LeaderboardAdapter(List<Leaderboard_Structure> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_rowleaderboard, parent, false);



        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Leaderboard_Structure movie = moviesList.get(position);
        holder.name.setText(movie.getName());
        holder.points.setText((String.valueOf(movie.getPoints())));
        holder.rank.setText((String.valueOf(movie.getRank())));

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }


}



