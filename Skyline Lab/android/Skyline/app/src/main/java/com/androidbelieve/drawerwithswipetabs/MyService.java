package com.androidbelieve.drawerwithswipetabs;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.widget.Toast;

import com.androidbelieve.kale.mqtt.Connection;
import com.androidbelieve.kale.mqtt.Connections;
import com.androidbelieve.kale.mqtt.MqttCallBackHandler;

import org.eclipse.paho.client.mqttv3.MqttConnectOptions;

public  class MyService extends Service {


    public static int n;
    MqttCallBackHandler bsl;
    Connection c;
    boolean connected;
    Connections connections;
    MqttCallBackHandler mh;
    public MyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        connections = new Connections(this);

        Toast.makeText(MyService.this, "Service started", Toast.LENGTH_SHORT).show();
        c = new Connection(this, getString(R.string.ip), getString(R.string.port), "service") {
            @Override
            public void Connected(boolean isConnected) {

            }
        };

        MqttConnectOptions mo = new MqttConnectOptions();
        mo.setMqttVersion(MqttConnectOptions.MQTT_VERSION_3_1);
        c.setMqttConnectOptions(mo);
        c.CreateConnection();

        c.getClient().setCallback(new MqttCallBackHandler() {
            @Override
            public void messageRecieved(String s) {

               if(s.contentEquals("intrusion")) {
                   push_notification("Safety break");
                   n = 1;
               }
               else if (s.contentEquals("touch")) {
                   push_notification("Safe Lock broken");
                   n = 2;
               }
               else if (s.contentEquals("off_human_manual")) {
                    push_notification("You Forgot to switch off your appliances");
                    n = 3;
                }
            }
        });
        c.connect();
        connections.getInstance().addConnection(c);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(MyService.this, "Service Destroyed", Toast.LENGTH_SHORT).show();
        c.disconnect();

    }

    public void push_notification(String s){
        // define sound URI, the sound to be played when there's a notification
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);

        // intent triggered, you can add other intent for other actions
        Intent intent = new Intent(MyService.this, Settings.class);
        PendingIntent pIntent = PendingIntent.getActivity(MyService.this, 0, intent, 0);

        // this is it, we'll build the notification!
        // in the addAction method, if you don't want any icon, just set the first param to 0
        Notification mNotification = new Notification.Builder(MyService.this)
                .setPriority(Notification.PRIORITY_HIGH)
                .setContentTitle("DANGER!!!")
                .setContentText(s)
          //      .setContentText("Danger at Home !!!!!! Someone just intruded ")
                .setSmallIcon(R.drawable.ic_notifications_black_24dp)
                .setContentIntent(pIntent)
                .setSound(soundUri)
                .setDefaults(Notification.DEFAULT_LIGHTS)
                .addAction(R.drawable.ic_notifications_black_24dp, "Off", pIntent)
                .addAction(0, "View", pIntent)
                .setAutoCancel(true)
                .build();



        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // If you want to hide the notification after it was selected, do the code below
        // myNotification.flags |= Notification.FLAG_AUTO_CANCEL;

        notificationManager.notify(0, mNotification);

    }
    public void setmqttcallbackhandler(MqttCallBackHandler hand) {
        bsl = hand;
    }


}

