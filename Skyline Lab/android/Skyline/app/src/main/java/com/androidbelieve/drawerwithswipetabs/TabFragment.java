package com.androidbelieve.drawerwithswipetabs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Ratan on 7/27/2015.
 */
public class TabFragment extends Fragment {

    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    public static int int_items = 3 ;
    public static int PRIMARY_FRAGMWNT = 0;
    public static int KITCHEN_FRAGMWNT = 1;
    public static int BEDROOM_FRAGMWNT = 2;

    public final int PRIMARY = 0;
    public final int KITCHEN = 1;
    public final int BEDROOM = 2;

    private PrimaryFragment p;
    private SocialFragment s;
    private UpdatesFragment u;
    TextView tv;
    Context c;

    public TabFragment() {

        p = new PrimaryFragment();
        s = new SocialFragment();
        u = new UpdatesFragment();
    }




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /**
         *Inflate tab_layout and setup Views.
         */
            View x =  inflater.inflate(R.layout.tab_layout,null);
        tabLayout = (TabLayout) x.findViewById(R.id.tabs);
        viewPager = (ViewPager) x.findViewById(R.id.viewpager);


        /**
         *Set an Apater for the View Pager
         */
        viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));

        /**
         * Now , this is a workaround ,
         * The setupWithViewPager dose't works without the runnable .
         * Maybe a Support Library Bug .
         */


        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                    tabLayout.setupWithViewPager(viewPager);
                   }
        });

        return x;






    }


    public void setText(int fragment, String s) {
        switch (fragment) {
            case PRIMARY:
                p.setTectView(s);
                break;
            case KITCHEN:
                this.s.setTectView(s);
                break;
            case BEDROOM:
                u.setTectView(s);
                break;
        }
    }
    class MyAdapter extends FragmentPagerAdapter{

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Return fragment with respect to Position .
         */





        @Override
        public Fragment getItem(int position)
        {
          switch (position){
              case 0 : return p;
              case 1 : return s;
              case 2 : return u;
          }
        return null;
        }

        @Override
        public int getCount() {

            return int_items;

        }

        /**
         * This method returns the title of the tab according to the position.
         */

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position){
                case 0 :
                    return "";
                case 1 :
                    return "";
                case 2 :
                    return "";

            }
                return null;
        }
    }



}
