package com.androidbelieve.kale.mqtt;

import android.content.Context;
import android.widget.Toast;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import java.io.UnsupportedEncodingException;


//  This is the class to create a connection between the android and the mqtt broker



    public abstract class Connection {

    private Context c = null;
     boolean con;
    private String clientHandle = null;
    private MqttAndroidClient client = null;
    private MqttConnectOptions mo = null;
    private String port = null;
    private String clientId = null;
    private MemoryPersistence persistence = null;
    private String ip = null;
    private Context this_class;

    public Connection(Context c, String ip, String port, String clientHandle) {
        this.c = c;
        this.ip = ip;
        this.clientHandle = new String(clientHandle);
        this.clientId = MqttClient.generateClientId();
        this.port = port;

    }


    public void setMqttConnectOptions(MqttConnectOptions mo) {
        this.mo = mo;
    }

    public void CreateConnection() {
        String url = "tcp://" +ip + ":" +port;
        client = new MqttAndroidClient(c, url, clientId, persistence);
    }



    public void setMemoryPersistence(MemoryPersistence mp) {
        this.persistence = mp;
    }

    public boolean getCOnnectionStatus() {
            return con;
    }

        public abstract void Connected(boolean isConnected);

    public void connect() {

        ActionListener al;
        try {
         IMqttToken token = client.connect(mo);
         token.setActionCallback(al = new ActionListener(c, ActionListener.Action.CONNECT, "handle") {
             @Override
             public void getResult(boolean r) {
                Toast.makeText(c, "Connected = " + r, Toast.LENGTH_SHORT).show();
                Connected(r);
                 con = r;
             }
         });
        }
        catch (MqttException e) {
            con = false;
        }
    }

    public String getHandle() {
        return this.clientHandle;
    }

    public String getclientid() {
        return this.clientId;
    }


    public MqttConnectOptions GetMqttConnectOptions() {
        return this.mo;
    }

    public boolean subscribe(String topic, int qos) {
        boolean k = false;
        try {
            client.subscribe(topic, qos);
            k = true;
        }catch (MqttException e) {
            k = false;
        }
        return k;
    }


    public boolean publish(String topic, String message, int qos) {
        boolean k = false;
        byte[] encode = new byte[0];
        try {
            encode = message.getBytes("UTF-8");
            MqttMessage mqmessage = new MqttMessage(encode);
            mqmessage.setQos(qos);
            client.publish(topic, mqmessage);
            k = true;
        }catch (UnsupportedEncodingException e) {
            k = false;
        }catch (MqttException e) {
            k = false;
        }
        return k;
    }
        public boolean unsubscribe(String topic) {
           boolean done = true;
            try {
               client.unsubscribe(topic);
           }catch (MqttException e) {
               done = false;
           }
            return done;
        }


    public MqttAndroidClient getClient() {
        return client;
    }


    public boolean disconnect() {
        boolean k = false;
        try {
            client.disconnect();
        }catch (MqttException e) {
            k = false;
        }
        return k;
    }
}
