package com.androidbelieve.kale.mqtt;

import android.content.Context;
import android.database.DataSetObservable;

import java.util.HashMap;

/**
 * Created by naveen on 6/12/2016.
 */
public class Connections {




    private static Connections instance = null;
    private HashMap<String, Connection> connectionlist;
    private  Context context;

    public Connections(Context c) {
        this.context = c;

    }


    private Connections() {
        connectionlist = new HashMap<String, Connection>();
    }

    public Connections getInstance() {
        if(instance == null) {
            instance = new Connections();
        }
        return instance;
    }

    public Connection getConnection(String clientHandle) {
        return connectionlist.get(clientHandle);
    }

    public void removeConnection(Connection c) {
        connectionlist.remove(c.getHandle());

    }

    public void addConnection(Connection c) {
        connectionlist.put(c.getHandle(), c);

    }


}
