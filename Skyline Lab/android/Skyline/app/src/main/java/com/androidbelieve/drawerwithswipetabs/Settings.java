package com.androidbelieve.drawerwithswipetabs;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.gesture.Prediction;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import com.androidbelieve.kale.mqtt.Connection;
import com.androidbelieve.kale.mqtt.Connections;
import com.androidbelieve.kale.mqtt.MqttCallBackHandler;

import java.util.prefs.Preferences;


public class Settings extends AppCompatActivity {

    Connection c;
    Connections a;
    Context ccc;

    SharedPreferences sp;
    SharedPreferences.Editor e;
    Switch safety,touch,smoke;
    Button buzzoff;
    MqttCallBackHandler mh;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = getSharedPreferences("switch_state",MODE_PRIVATE);
        e = sp.edit();
        a = new Connections(this);
        c = a.getInstance().getConnection("service");



        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        safety = (Switch)findViewById(R.id.switch_safety);
        touch = (Switch)findViewById(R.id.sw_touch);
        smoke = (Switch)findViewById(R.id.sw_smoke);
        buzzoff = (Button) findViewById(R.id.sw_buzzeroff);

        safety.setChecked(sp.getBoolean("safety",false));
        touch.setChecked(sp.getBoolean("touch",false));
       smoke.setChecked(sp.getBoolean("smoke",false));





        safety.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (c.getCOnnectionStatus()){
                    if (isChecked) {
                        c.subscribe("ir",0);
                        c.publish("senir_auto", "1", 0);
                    }
                    else {
                        c.publish("senir_auto", "0", 0);
                        c.unsubscribe("ir");
                        removeNotification();

                    }
                }
                else {
                    Toast.makeText(Settings.this, "Connection Error", Toast.LENGTH_SHORT).show();

                }
            }
        });

        touch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (c.getCOnnectionStatus()){
                    if (isChecked) {
                        c.subscribe("touch",0);
                        c.publish("sentouch_auto", "1", 0);
                    }

                    else {
                        c.publish("sentouch_auto", "0", 0);
                        c.unsubscribe("touch");
                        removeNotification();
          //              c.publish("buzzeroff_touch","1",0);
                    }
                }
                else {
                    Toast.makeText(Settings.this, "Connection Error", Toast.LENGTH_SHORT).show();
                }

            }
        });

        smoke.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (c.getCOnnectionStatus()){
                    if (isChecked) {
                    }
                    else {
                        c.publish("sensmoke_auto", "0", 0);
                    }
                }
                else {
                    Toast.makeText(Settings.this, "Connection Error", Toast.LENGTH_SHORT).show();
                }
            }
        });

        buzzoff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeNotification();

                if(MyService.n == 1){
                    c.publish("buzzeroff_ir","1",0);
                }
                else if(MyService.n == 2){
                    c.publish("buzzeroff_touch","1",0);
                }
                else {
                    Toast.makeText(Settings.this, "Error", Toast.LENGTH_SHORT).show();
                }
            }
        });

        }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        e.putBoolean("safety",safety.isChecked());
        e.putBoolean("touch",touch.isChecked());
        e.putBoolean("smoke",smoke.isChecked());
        e.commit();
    }


    public void removeNotification() {
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancel(0);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}
