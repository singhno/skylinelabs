package com.androidbelieve.drawerwithswipetabs;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.androidbelieve.kale.mqtt.Connection;
import com.androidbelieve.kale.mqtt.Connections;
import com.androidbelieve.kale.mqtt.MqttCallBackHandler;

import java.util.ArrayList;


/**
 * Created by Shayan 7/29/2015.
 */
public class PrimaryFragment extends Fragment {
    protected static final int RESULT_SPEECH = 1;
    View a;
    FloatingActionButton b;
    private final int qos = 0;
    Connections connections;
    ImageButton ib;
    TextView tv;
    ToggleButton tb;
    Connection connection,service;
    String voice;
    public PrimaryFragment() {

        connections = new Connections(null);
        connection = connections.getInstance().getConnection("client");
        service = connections.getInstance().getConnection("service");
    }

    public View getMyview() {
        return a;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        a = inflater.inflate(R.layout.primary_layout, container, false);
        tv = (TextView) a.findViewById(R.id.templivcold);
        b = (FloatingActionButton) a.findViewById(R.id.fab_liv);
        tb = (ToggleButton) a.findViewById(R.id.switch_temp);


        ib = (ImageButton) a.findViewById(R.id.btnSpeak);
        ib.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(
                        RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");

                try {
                    startActivityForResult(intent, RESULT_SPEECH);
                } catch (ActivityNotFoundException a) {

                }
            }
        });


        tb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tb.isChecked()) {
                   service.publish("auto","1",0);
                    Toast.makeText(getActivity(), "published auto with 1", Toast.LENGTH_SHORT).show();
                }
                else {
                    service.publish("auto","0",0);
                    service.subscribe("off_human_manual",0);
                    Toast.makeText(getActivity(), "published auto with 0  off_..and subscribed", Toast.LENGTH_SHORT).show();
                }
            }
        });
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getActivity(), Appliances.class);
                startActivity(i);
            }
        });
        return a;
    }

/*
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case RESULT_SPEECH: {
                if (resultCode == Activity.RESULT_OK && null != data) {

                    ArrayList<String> text = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                        voice = text.get(0).toLowerCase();
                        tv.setText(voice);
                }
                break;
            }

        }
    String message = null;
    String topic = null;
        if(voice.contains("on")) {
            message = "1";
            if(voice.contains("fan")) {
                topic = "fan";
            }
            else if(voice.contains("ac") || (voice.contains("air conditioner"))) {
                topic = "ac";
            }else if(voice.contains("cfl")) {
                topic = "cfl";
            }
            connection.publish(topic, message, qos);
        }

        else if(voice.contains("off")) {
            message = "0";
            if(voice.contains("fan")) {
                topic = "fan";
            }
            else if(voice.contains("ac") || (voice.contains("air conditioner"))) {
                topic = "ac";
            }else if(voice.contains("cfl")) {
                topic = "cfl";
            }
            connection.publish(topic, message, qos);
        }

        else {

            Toast.makeText(getContext(), "Theek se bol na bhaii", Toast.LENGTH_SHORT).show();
        }

    }

*/
    public void setTectView(String s) {
        tv.setText(s);
    }
}