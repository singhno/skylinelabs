package com.androidbelieve.drawerwithswipetabs;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.androidbelieve.kale.mqtt.Connection;
import com.androidbelieve.kale.mqtt.Connections;

/**
 * Created by Shayan
 */
public class UpdatesFragment extends Fragment {

    View a;
    private final int qos = 0;
    Connections connections;
    ImageButton ib;
    TextView tv;
    ToggleButton tb;
    Connection connection;
    String voice;

    FloatingActionButton b;


    public UpdatesFragment() {
        // Required empty public constructor
        connections = new Connections(null);
        connection = connections.getInstance().getConnection("client");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View a =  inflater.inflate(R.layout.updates_layout, container, false);
        b = (FloatingActionButton) a.findViewById(R.id.fab_bedroom);
        tv =(TextView)a.findViewById(R.id.tempbedroomcold);
        tb = (ToggleButton) a.findViewById(R.id.tb_bedroom);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i  = new Intent(getActivity(),Appliances_Bedroom.class);
                startActivity(i);
            }
        });

        tb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tb.isChecked()) {
                    connection.subscribe(getString(R.string.temperature), qos);
                } else {
                    connection.unsubscribe(getString(R.string.temperature));
                }
            }
        });
        return a;
    }
    public void setTectView(String s) {
        tv.setText(s);
    }

}