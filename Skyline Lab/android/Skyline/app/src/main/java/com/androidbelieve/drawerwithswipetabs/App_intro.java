package com.androidbelieve.drawerwithswipetabs;

import android.app.ActivityManager;
import android.app.backup.SharedPreferencesBackupHelper;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.androidbelieve.kale.mqtt.Connection;

public class App_intro extends ActionBarActivity {

    TextView skip, next;
    SharedPreferences sp;
    SharedPreferences.Editor e;
    Connection c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_intro);
        sp = getSharedPreferences("switch_state",MODE_PRIVATE);
        e = sp.edit();

        boolean a = sp.getBoolean("shayan", false);
        if(a == true){

            Toast.makeText(App_intro.this, "already running", Toast.LENGTH_SHORT).show();

        }
        else {
            startService(new Intent(App_intro.this, MyService.class));

        }
        boolean j = isMyServiceRunning(MyService.class);
        Toast.makeText(App_intro.this, ""+j, Toast.LENGTH_SHORT).show();
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        skip = (TextView) findViewById(R.id.skip);
        next = (TextView) findViewById(R.id.next);

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(App_intro.this,LoginActivity.class);
                startActivity(i);
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }






        final ViewPager customViewpager = (ViewPager) findViewById(R.id.viewpager_custom);
        CircleIndicator customIndicator = (CircleIndicator) findViewById(R.id.indicator_custom);
        DemoPagerAdapter customPagerAdapter = new DemoPagerAdapter(getSupportFragmentManager());
        customViewpager.setAdapter(customPagerAdapter);
        customIndicator.setViewPager(customViewpager);

         customViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {
                if(customViewpager.getCurrentItem()==4) {
                    next.setText("Done");
                    next.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                          Intent i = new Intent(App_intro.this,LoginActivity.class);
                            startActivity(i);

                        }
                    });
                }

            }

            @Override
            public void onPageSelected(int i) {

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        if(customViewpager.getCurrentItem()!=5) {
            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    customViewpager.setCurrentItem(customViewpager.getCurrentItem() + 1);
                 }

            });

        }


        customViewpager.setPageTransformer(true, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View view, float position) {
                // Ensures the views overlap each other.
                view.setTranslationX(view.getWidth() * -position);

                // Alpha property is based on the view position.
                if (position <= -1.0F || position >= 1.0F) {
                    view.setAlpha(0.0F);
                } else if (position == 0.0F) {
                    view.setAlpha(1.0F);
                } else { // position is between -1.0F & 0.0F OR 0.0F & 1.0F
                    view.setAlpha(1.0F - Math.abs(position));
                }


            }
        });
            }
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        e.putBoolean("shayan",isMyServiceRunning(MyService.class));
        e.commit();
    }
}

