package com.androidbelieve.kale.mqtt;


import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.util.ArrayList;

public abstract  class MqttCallBackHandler implements MqttCallback{


    private ArrayList<String> messages = null;

    public  MqttCallBackHandler() {
            messages = new ArrayList<String>();
    }

    public ArrayList<String> getAllmessages() {
        return this.messages;
    }

    public abstract void messageRecieved(String s);

    @Override
    public void connectionLost(Throwable throwable) {

    }

    @Override
    public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
        String messageString = new String(mqttMessage.getPayload());
        messages.add(messageString);
        messageRecieved(messageString);
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

    }
}
