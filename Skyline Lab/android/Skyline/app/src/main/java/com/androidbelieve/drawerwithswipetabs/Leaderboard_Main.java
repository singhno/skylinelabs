package com.androidbelieve.drawerwithswipetabs;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class Leaderboard_Main extends AppCompatActivity {

    private List<Leaderboard_Structure> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private LeaderboardAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaderboard);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_leaderboard);

        mAdapter = new LeaderboardAdapter(movieList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        prepareMovieData();

    }

    private void prepareMovieData() {
        Leaderboard_Structure movie = new Leaderboard_Structure("Mad Max: Fury Road", 1, 250);
        movieList.add(movie);

        movie = new Leaderboard_Structure("Inside Out", 2, 250);
        movieList.add(movie);

        movie = new Leaderboard_Structure("Inside Out", 2, 250);
        movieList.add(movie);

        movie = new Leaderboard_Structure("Inside Out", 2, 250);
        movieList.add(movie);

        movie = new Leaderboard_Structure("Inside Out", 2, 250);
        movieList.add(movie);

        movie = new Leaderboard_Structure("Inside Out", 2, 250);
        movieList.add(movie);

        movie = new Leaderboard_Structure("Inside Out", 2, 250);
        movieList.add(movie);


        mAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }


}
