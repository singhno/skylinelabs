/*******************************************************************************
 * Copyright (c) 1999, 2014 IBM Corp.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution. 
 *
 * The Eclipse Public License is available at 
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at 
 *   http://www.eclipse.org/org/documents/edl-v10.php.
 */
package com.androidbelieve.kale.mqtt;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;

import android.content.Context;
import android.widget.Toast;

public abstract class  ActionListener implements IMqttActionListener {

    enum Action {
        /** Connect Action **/
        CONNECT,
        /** Disconnect Action **/
        DISCONNECT,
        /** Subscribe Action **/
        SUBSCRIBE,
        /** Publish Action **/
        PUBLISH
    }
    private Action action;
    /** The arguments passed to be used for formatting strings**/
    private boolean k = false;
    /** Handle of the {@link Connection} this action was being executed on **/
    private String clientHandle;
    /** {@link Context} for performing various operations **/
    private Context context;
    public ActionListener(Context context, Action action,
                          String clientHandle) {
        this.context = context;
        this.action = action;
        this.clientHandle = clientHandle;
    }

     public abstract void getResult(boolean r);


    @Override
    public void onSuccess(IMqttToken asyncActionToken) {
        this.k = true;
        getResult(this.k);
    }

    @Override
    public void onFailure(IMqttToken token, Throwable exception) {
        k = false;
        getResult(this.k);
    }



}