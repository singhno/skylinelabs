function plotData(x, y)


figure;
plot(x, y, 'rx', 'MarkerSize', 10);
ylabel('Sleeping time of individual');
xlabel('Day');

end
