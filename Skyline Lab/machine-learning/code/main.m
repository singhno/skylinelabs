

%% Initialization
clear ; close all; clc

fprintf('Plotting Data ...\n')
data = load('data.txt');
X = data(:, 1); y = data(:, 2);
m = length(y); % number of training examples

plotData(X, y);


fprintf('Running Gradient Descent ...\n')

X = [ones(m, 1), data(:,1)]; % Add a column of ones to x
theta = zeros(2, 1); % initialize fitting parameters

% Some gradient descent settings
iterations = 1500;
alpha = 0.01;

% compute and display initial cost
computeCost(X, y, theta)

% run gradient descent
theta = gradientDescent(X, y, theta, alpha, iterations);


% Plot the linear fit
hold on; % keep previous plot visible
plot(X(:,2), X*theta, '-')
legend('Training data', 'Linear regression')
hold off % don't overlay any more plots on this figure

k1=input('Enter the day')
predict1 = [1, k1] *theta;
fprintf("The time will be %f\n", predict1);
k2=input('Enter the day')
predict2 = [1, k2] * theta;
fprintf("The time will be %f\n",predict2);

